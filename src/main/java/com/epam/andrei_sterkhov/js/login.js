$(document).ready(function () {
    $("#enter").click(function () {
        var login = $("#login").val();
        var pass = $("#pass").val();

        var isValidate = false;

        if (login === "") {
            $(".warn.login span").text("Поле Логин не должно быть пустым");
            $(".warn.login span").show();
        } else {
            $("#login").val(login);
            $(".warn.login span").hide();
        }

        if (pass === "") {
            $(".warn.pass span").text("Поле Пароль не должно быть пустым");
            $(".warn.pass span").show();
        } else {
            $(".warn.pass span").hide();
        }

        if(login === "John" && pass === "12345"){
            isValidate = true;
        } else {
            if(login && pass){
                $(".warn.pass span").text("Неверный логин или пароль");
                $(".warn.pass span").show();
            }
        }

        return isValidate;
    });
});